export class UrlMapRule {
  uuid: string;
  pattern: string;
  access: string;
  httpMethods: string;
}
